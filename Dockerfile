FROM node:20.14.0-alpine3.19

WORKDIR /app

ENV PATH="$PATH:/app/node_modules/.bin"

RUN apk --update --no-cache add \
    bash=5.2.21-r0 \
    curl=8.5.0-r0 \
    git=2.43.4-r0 \
    git-lfs=3.4.1-r3 \
    openssh=9.6_p1-r0

RUN npm install --location=global \
        semantic-release@24.0.0 \
        @semantic-release/changelog@6.0.3 \
        @semantic-release/commit-analyzer@13.0.0 \
        @semantic-release/exec@6.0.3 \
        @semantic-release/git@10.0.1 \
        @semantic-release/gitlab@13.1.0 \
        @semantic-release/npm@12.0.1 \
        @semantic-release/release-notes-generator@14.0.0 \
        @commitlint/cli@19.3.0 \
        @commitlint/config-conventional@19.2.2 \
        conventional-changelog-conventionalcommits@8.0.0

COPY --from=jnorwood/helm-docs:v1.13.1 [ "/usr/bin/helm-docs", "/usr/bin/helm-docs" ]

ENTRYPOINT [ "/bin/bash" ]
CMD [ "semantic-release" ]
