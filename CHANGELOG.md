# Semantic Versioning Changelog

## [1.4.3](https://gitlab.com/nuageit-community/smr-toolbox/compare/1.4.2...1.4.3) (2024-06-22)


### :bug: Fixes

* install packages ([f1d1598](https://gitlab.com/nuageit-community/smr-toolbox/commit/f1d1598fa50c6fb3dddfea82bc6ddbae5fb0cecc))
* organize project ([1b23c5e](https://gitlab.com/nuageit-community/smr-toolbox/commit/1b23c5ebe77d3957d67846e7ac54a30b4d88def1))
* organize project ([df70f95](https://gitlab.com/nuageit-community/smr-toolbox/commit/df70f955125b51746a20e7f97e34dac349f69a7f))
* organize project ([507b889](https://gitlab.com/nuageit-community/smr-toolbox/commit/507b889d1ae3300bc38a3fc3bdede0df941b9ad5))
* organize project ([8b51232](https://gitlab.com/nuageit-community/smr-toolbox/commit/8b51232335f835d21d59fd886ca8a6bca50c3732))
* organize project ([3698898](https://gitlab.com/nuageit-community/smr-toolbox/commit/36988981393cd1e4c83bf3a8c796eb85e3093b0e))
* organize project ([e6cfa48](https://gitlab.com/nuageit-community/smr-toolbox/commit/e6cfa48342b149654d8135196e65937da74a3fbc))
* organize project ([2e6da93](https://gitlab.com/nuageit-community/smr-toolbox/commit/2e6da93849060442288b567c0a8ab5528b412329))
* organize project ([e352af9](https://gitlab.com/nuageit-community/smr-toolbox/commit/e352af9aa70b011680cd3117d323f7880830afa9))
* organize project ([19f5c9a](https://gitlab.com/nuageit-community/smr-toolbox/commit/19f5c9a373f66f18f048a85f05ee4687c650256e))
* organize project ([d4d0225](https://gitlab.com/nuageit-community/smr-toolbox/commit/d4d0225ccde2acfa4a4e78d05be9838b6ade6c51))
* organize project ([b077402](https://gitlab.com/nuageit-community/smr-toolbox/commit/b0774023a4b9eb175c1964b834c13d9a391e4270))
* organize project ([7a0e487](https://gitlab.com/nuageit-community/smr-toolbox/commit/7a0e487df64f3b38fb0632b78aec7fe01b94846d))
* organize project ([a3303b6](https://gitlab.com/nuageit-community/smr-toolbox/commit/a3303b674dabe071541eb8dc7b48a0de844fcb84))
* organize project ([1a34c84](https://gitlab.com/nuageit-community/smr-toolbox/commit/1a34c84c80c8fc5c3304e05f7ecc50269b60a9a7))
* organize project ([4b6c7b9](https://gitlab.com/nuageit-community/smr-toolbox/commit/4b6c7b9b3ab671ab3dd805bac77fed2e6c0a44ca))
* organize project ([4d512c1](https://gitlab.com/nuageit-community/smr-toolbox/commit/4d512c155e1d46d6a9f027de91964e63862b057b))
* organize project ([0b7a4c4](https://gitlab.com/nuageit-community/smr-toolbox/commit/0b7a4c4dbd45c3650762acd490ff6579ea90b6c3))

## [1.4.2](https://gitlab.com/nuageit-community/smr-toolbox/compare/1.4.1...1.4.2) (2024-01-12)


### :bug: Fixes

* image tools ([a343167](https://gitlab.com/nuageit-community/smr-toolbox/commit/a343167263921db0ad9f50159b9f9576ea8ccd20))

## [1.4.1](https://gitlab.com/nuageit-community/smr-toolbox/compare/1.4.0...1.4.1) (2024-01-12)


### :bug: Fixes

* lib commitlint ([90d62db](https://gitlab.com/nuageit-community/smr-toolbox/commit/90d62db68f6e36fc18e793a0cd9ac07ef266df8c))

## [1.4.0](https://gitlab.com/nuageit-community/smr-toolbox/compare/1.3.0...1.4.0) (2024-01-12)


### :zap: Refactoring

* update file name ([e476725](https://gitlab.com/nuageit-community/smr-toolbox/commit/e476725806ec6c338dd8c1e39d92d5332556fe68))


### :repeat: CI

* add include job ([4f0cd6d](https://gitlab.com/nuageit-community/smr-toolbox/commit/4f0cd6d2afc56dee9034b89fd15d515286b70aa1))
* change include ([58d99e0](https://gitlab.com/nuageit-community/smr-toolbox/commit/58d99e00fc830f07e89ab56baf1aa81faa09e548))
* change ref name ([99b0aca](https://gitlab.com/nuageit-community/smr-toolbox/commit/99b0aca6837524eb1f71885e41dcb96fc138d108))


### :memo: Docs

* pretty readme ([fd6da62](https://gitlab.com/nuageit-community/smr-toolbox/commit/fd6da62cdb5aa0b235715521fb945589708ce859))
* pretty readme ([52e3a54](https://gitlab.com/nuageit-community/smr-toolbox/commit/52e3a544f689603152caf48e86e2a5cf9de11f5b))
* restyle readme ([0b70a16](https://gitlab.com/nuageit-community/smr-toolbox/commit/0b70a1693d303b0016ef9a25f3d0dc9d1e218c7b))


### :bug: Fixes

* code dockerfile versions ([e8db7b9](https://gitlab.com/nuageit-community/smr-toolbox/commit/e8db7b948224170180beafe62b0ae845a1ae54cb))
* dockerfile setup libs version ([c0eea1d](https://gitlab.com/nuageit-community/smr-toolbox/commit/c0eea1dec54613f03dce387e3f022e1072f022a5))
* gitleaks config ([c97cb4b](https://gitlab.com/nuageit-community/smr-toolbox/commit/c97cb4bb03a6ea35087d82356f8efc4d137fc3d2))
* hooks setup ([ad8bf7d](https://gitlab.com/nuageit-community/smr-toolbox/commit/ad8bf7d87aa2a1d9be0312fade8260eade74ecc7))
* identation code ([db71511](https://gitlab.com/nuageit-community/smr-toolbox/commit/db71511e58019f34a6d9b3bc65da627c0c1e7833))
* identation yaml ([d192dbd](https://gitlab.com/nuageit-community/smr-toolbox/commit/d192dbdbc283f3965b181a19dfd5889492a28119))
* organize project setup ([d8e64b8](https://gitlab.com/nuageit-community/smr-toolbox/commit/d8e64b8ec2cefa011baa4976b587388a48171700))
* pre-commit config ([411c129](https://gitlab.com/nuageit-community/smr-toolbox/commit/411c129f4cc6029efb33d516d123ad2d0066576b))
* taskfile script with hadolint ([26cd75e](https://gitlab.com/nuageit-community/smr-toolbox/commit/26cd75e608f24b8dbe938b45c829a370372cdd1f))

## [1.3.0](https://gitlab.com/nuageit-community/smr-toolbox/compare/1.2.1...1.3.0) (2023-01-18)


### :bug: Fixes

* setup configurations ([9c98f94](https://gitlab.com/nuageit-community/smr-toolbox/commit/9c98f94eaf976b9cb6c1ed9130502976b9cfa90a))


### :memo: Docs

* link git clone ([3d45285](https://gitlab.com/nuageit-community/smr-toolbox/commit/3d4528585003650943a54420b43c52da98d9981e))
* remove topic ([f4d134a](https://gitlab.com/nuageit-community/smr-toolbox/commit/f4d134a67c25046b11f5ac78405e4a2938731948))


### :sparkles: News

* add google semantic release replace ([69b08e2](https://gitlab.com/nuageit-community/smr-toolbox/commit/69b08e2900108783a54813a8d2c8ad1ac2194c55))

## [1.2.1](https://gitlab.com/nuageit-community/smr-toolbox/compare/1.2.0...1.2.1) (2023-01-17)


### :memo: Docs

* more information ([fdd774d](https://gitlab.com/nuageit-community/smr-toolbox/commit/fdd774d2c9213034780c5a3ec0f965b6dcf5a9bc))
* resize image ([efd7462](https://gitlab.com/nuageit-community/smr-toolbox/commit/efd746231d42fe9c6a13224b8907d0a356e07907))


### :repeat: CI

* adicionando novo template pipeline ([94ed50e](https://gitlab.com/nuageit-community/smr-toolbox/commit/94ed50ed94f674dbb40988483b81cd0fc05d4dee))
* correcao include ([91b393c](https://gitlab.com/nuageit-community/smr-toolbox/commit/91b393c2f2bdf68031b6fac40e882a0b6d2ce8e0))

## [1.2.0](https://gitlab.com/nuageit/devops/smr-toolbox/compare/1.1.1...1.2.0) (2022-11-27)


### :sparkles: News

* add semantic release exec ([1e94241](https://gitlab.com/nuageit/devops/smr-toolbox/commit/1e94241326120d26179184457fd712a7bd55daa1))

## [1.1.1](https://gitlab.com/nuageit/devops/smr-toolbox/compare/1.1.0...1.1.1) (2022-11-27)


### :bug: Fixes

* dockerfile plugin version ([31e9e5c](https://gitlab.com/nuageit/devops/smr-toolbox/commit/31e9e5c21758f29f1021a4a918b29a3194b446c8))

## [1.1.0](https://gitlab.com/nuageit/devops/smr-toolbox/compare/1.0.0...1.1.0) (2022-11-27)


### :sparkles: News

* add plugin google replace ([1a7db15](https://gitlab.com/nuageit/devops/smr-toolbox/commit/1a7db1538a9b04a911347e916cc1a3b041133f7d))

## 1.0.0 (2022-11-27)


### :bug: Fixes

* dockerfile semantic release plugins versions ([2665fd2](https://gitlab.com/nuageit/devops/smr-toolbox/commit/2665fd271668c072d4fa3b0bdffd25646371bbf5))


### :sparkles: News

* add config files and dockerfile ([76cbd79](https://gitlab.com/nuageit/devops/smr-toolbox/commit/76cbd79cb469f5304d433acc8cc8b533300e28e5))
* add gitignore, editorconfig and pipeline ([c07907d](https://gitlab.com/nuageit/devops/smr-toolbox/commit/c07907d66bb44105b0b72b9a517a7c362f092400))
* add helm-docs ([3c2451f](https://gitlab.com/nuageit/devops/smr-toolbox/commit/3c2451fe9520dd282d833dbee0e98c34c0c681b3))


### :memo: Docs

* add content ([7378d48](https://gitlab.com/nuageit/devops/smr-toolbox/commit/7378d48957059620f337f814d3789cf71dc3eae7))
* empty readme ([f468539](https://gitlab.com/nuageit/devops/smr-toolbox/commit/f46853957f4fe9e0037ca1ffbbac046ade60b303))
* pretty content ([af513d6](https://gitlab.com/nuageit/devops/smr-toolbox/commit/af513d67be56e966434ba257d5d67aa51d6f609d))
